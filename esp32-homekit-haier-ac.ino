#include <Arduino.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <ir_Haier.h>
#include <HomeSpan.h>

const uint16_t kIrLed = 15;


struct HaierAC : Service::Thermostat {
  IRHaierAC160 ac;
  SpanCharacteristic *current_state;
  SpanCharacteristic *target_state;
  SpanCharacteristic *current_temp;
  SpanCharacteristic *target_temp;
  SpanCharacteristic *unit;
  float saved_temp;
  float temperature;
  int previous_ac_state;
  int saved_state;
  int ir_led_pin;
  HaierAC(int ir_led_pin) : Service::Thermostat(), ac(ir_led_pin, true) {
    saved_temp = 16;
    temperature = saved_temp;
    previous_ac_state = 0;
    saved_state = 0;
    ac.begin();
    ac.off();
    ac.send();
    current_state = new Characteristic::CurrentHeatingCoolingState(0);
    target_state = new Characteristic::TargetHeatingCoolingState(saved_state);
    current_temp = new Characteristic::CurrentTemperature(temperature);
    target_temp = (new Characteristic::TargetTemperature(saved_temp))->setRange(16, 30, 0.5);
    unit = new Characteristic::TemperatureDisplayUnits(0);
  }
  void updateSensor() {
    temperature = saved_temp;
    current_temp->setVal(temperature);
  }
  void toggleAC() {
    int state = target_state->getNewVal();
    if (state != 0) {
      previous_ac_state = state;
      target_state->setVal(0);
    } else {
      target_state->setVal(previous_ac_state);
    }
    update();
  }
  boolean update() {
    /*
    if ( power->getNewVal() != 0 )
    {
      Serial.println("A/C ON");
      ac.on();
      ac.send();
    }
    else
    {
      Serial.println("A/C OFF");
      ac.off();
      ac.send();
    }
    */
    int state = target_state->getNewVal();
    float temp = target_temp->getNewVal();

    if (state == 0) {
      ac.off();
    } else {
      ac.on();
      previous_ac_state = state;
    }

    switch (state) {
      case 0:
        break;
      case 1:
        ac.setMode(kHaierAcHeat);
        current_state->setVal(state);
        break;
      case 2:
        ac.setMode(kHaierAcCool);
        current_state->setVal(state);
        break;
      default:
        ac.setMode(kHaierAcAuto);
        // Setting the current state to auto cause the device to stop responding.
        current_state->setVal(0);
        break;
    }

    //ac.setFan(kPanasonicAcFanAuto);
    //ac.setSwingVertical(kPanasonicAcSwingVAuto);
    //ac.setSwingHorizontal(kPanasonicAcSwingHAuto);
    ac.setTemp(temp);
    ac.send();
  
    return true;
  }
};

void setup() {
  Serial.begin(115200);
  delay(200);

  Serial.println("Testing IR LED");
  pinMode(kIrLed, OUTPUT);
  digitalWrite(kIrLed, LOW);
  delay(2000);
  digitalWrite(kIrLed, HIGH);

  homeSpan.setWifiCallback([](){homeSpan.setPairingCode("11122333");});
  homeSpan.begin(Category::AirConditioners,"Haier AC");
  new SpanAccessory();
    new Service::AccessoryInformation();
      new Characteristic::Identify();
    new HaierAC(kIrLed);
}

void loop() {
  homeSpan.poll();
}
